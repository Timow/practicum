from Routes import *

roads = (
    ("AB", "A", "B", 10, 10),
    ("AB2", "A", "B", 3, 10),
    ("AB3", "A", "B", 11, 10)
)

net = init_network(roads)
print "running noneTests.."
assert get_length_of_route(net, []) == 0
assert get_length_of_route(net, ['CD']) is None

assert get_time_of_route(net, []) == 0
assert get_time_of_route(net, ['CD']) is None

assert get_cities_of_route(net, []) == []
assert get_cities_of_route(net, ['CD']) is None

assert cities_occur_in_route(net, ['A', 'C'], ['AB', 'CD']) is None
assert is_correct_route(net, ['AB', 'CD']) is None

assert get_start(net, 'LOL') is None
assert get_end(net, 'LOL') is None
assert get_time(net, 'LOL') is None
assert get_length(net, 'LOL') is None
assert get_speed(net, 'LOL') is None

assert get_all_roads_starting_from(net, 'LOL') is None

assert get_road_to(net, 'LOL', 'A') is None
assert get_road_to(net, 'A', 'LOL') is None
assert get_road_to(net, 'LOL', 'LOL') is None
assert get_road_to(net, 'A', 'B') == 'AB2'

assert get_shortest_route_floyd(net, 'LOL', 'LOL', ['A']) is None
assert get_shortest_route_floyd(net, 'A', 'LOL') is None
assert get_shortest_route_floyd(net, 'LOL', 'A', ['LOL']) is None
assert get_shortest_route_floyd(net, 'A', 'B', ['LOL']) is not None

assert get_quickest_route_via(net, 'LOL', 'A', []) is None
assert get_quickest_route_via(net, 'A', 'LOL', []) is None
assert get_quickest_route_via(net, 'LOL', 'LOL', []) is None
assert get_quickest_route_via(net, 'A', 'B', ['LOL']) is None

print "OK"
