import random

def get_rand_name(length):
    name = ''
    for i in range(length):
        char = chr(random.randint(ord('A'), ord('Z')))
        name += char
    return name

def gen_random_cities(num_cities):
    cities = set()
    max_length = num_cities // 26 + 1
    for i in range(num_cities):
        name = get_rand_name(max_length)
        while name in cities:
            name = get_rand_name(max_length)
        cities.add(name)
    return list(cities)

def gen_random_roads(num_cities, num_roads, min_length, max_length, min_speed, max_speed):
    cities = gen_random_cities(num_cities)
    len_cities = len(cities) - 1
    roads = []
    road_ids = set()
    for i in range(num_roads):
        start = cities[random.randint(0, len_cities)]
        end = cities[random.randint(0, len_cities)]
        while start == end:
            end = cities[random.randint(0, len_cities)]

        road_id = start + end
        while road_id in road_ids:
            road_id += "'"
        road_ids.add(road_id)
        length = random.randint(min_length, max_length)
        speed = float(random.randint(min_speed*4, max_speed*4)) / 4.0
        roads.append((road_id, start, end, length, speed))
    print cities
    return tuple(roads)
