from Routes import *
import time

network = (
    ("YOLOSTRADE","Antwerpen","Sydney",22345.0,1250),
("YOLOWAY","Sydney","Antwerpen",22345.0,1250),
("E17 AG","Antwerpen","Gent",47.5,120),
("E17 GA","Gent","Antwerpen",47.5,120),
("E34 AB","Antwerpen","Brugge",80.0,120),
("E34 BA","Brugge","Antwerpen",80.0,120),
("E40 BO","Brugge","Oostende",15.0,120),
("E40 OB","Oostende","Brugge",15.0,120),
("E403 BK","Brugge","Kortrijk",35.0,120),
("E403 KB","Kortrijk","Brugge",35.0,120),
("E40 BG","Brugge","Gent",40.0,120),
("E40 GB","Gent","Brugge",40.0,120),
("E17 KG","Kortrijk","Gent",38.0,120),
("E17 GK","Gent","Kortrijk",38.0,120),
("E40 GB","Gent","Brussel",41.0,120),
("E40 BG2","Brussel","Gent",41.0,120),
("E19 AB","Antwerpen","Brussel",32.56,120),
("E19 BA","Brussel","Antwerpen",32.56,120),
("A12 AB","Antwerpen","Brussel",29.0,90),
("A12 BA","Brussel","Antwerpen",29.0,90),
("E34 AT","Antwerpen","Turnhout",42.25,120),
("E34 TA","Turnhout","Antwerpen",42.25,120),
("E313 AH","Antwerpen","Hasselt",67.0,120),
("E313 HA","Hasselt","Antwerpen",67.0,120),
("A3 LB","Leuven","Brussel",27.0,120),
("A3 BL","Brussel","Leuven",27.0,120),
("E314 LH","Leuven","Hasselt",45.3,120),
("E314 HL","Hasselt","Leuven",45.3,120),
("E40 LeL","Leuven","Luik",62.3,120),
("E40 LLe","Luik","Leuven",62.3,120),
("E313 LH","Luik","Hasselt",55.37,120),
("E313 HL","Hasselt","Luik",55.37,120),
("N10 AL","Antwerpen","Lier",16.0,70),
("N10 LA","Lier","Antwerpen",16.0,70),
("N14 LM","Lier","Mechelen",23.0,70),
("N14 ML","Mechelen","Lier",23.0,70),
("N10 LAa","Lier","Aarschot",26.0,70),
("N10 AaL","Aarschot","Lier",26.0,70),
("N19 AL","Aarschot","Leuven",23.5,70),
("N19 LA","Leuven","Aarschot",23.5,70),
("N75 HG","Hasselt","Genk",33.0,70),
("N75 GH","Genk","Hasselt",33.0,70),
("E25 AL","Arlon","Luik",103.0,120),
("E25 LA","Luik","Arlon",103.0,120),
("E411 AN","Arlon","Namen",82.6,120),
("E411 NA","Namen","Arlon",82.6,120),
("E42 NL","Namen","Luik",57.0,120),
("E42 LN","Luik","Namen",57.0,120),
("E411 NW","Namen","Waver",34.0,120),
("E411 WN","Waver","Namen",34.0,120),
("E411 WB","Waver","Brussel",21.0,120),
("E411 BW","Brussel","Waver",21.0,120),
("E19 CB","Charleroi","Brussel",46.0,120),
("E19 BC","Brussel","Charleroi",46.0,120),
("E42 CN","Charleroi","Namen",31.0,120),
("E42 NC","Namen","Charleroi",31.0,120),
("E42 CB","Charleroi","Bergen",23.0,120),
("E42 BC","Bergen","Charleroi",23.0,120),
("E42 TB","Tournai","Bergen",29.0,120),
("E42 BT","Bergen","Tournai",29.0,120),
("E403 KT","Kortrijk","Tournai",31.5,120),
("N403 TK","Tournai","Kortrijk",31.5,120),
("HighWayToHell","Charleroi","Hel",666.666,666),
("2deKans","Hel","Charleroi",666.666,666),
("StairwayToHeaven","Turnhout","Hemel",777.777,777),
("BackToReality","Hemel","Turnhout",777.777,777))

net = init_network(network)
print "big tests.."

a = time.time()
assert get_quickest_route_via(net, "Turnhout", "Bergen", ["Brussel"]) == ['E34 TA', 'E19 AB', 'E19 BC', 'E42 CB']
b = time.time()
assert get_quickest_route_via(net, "Kortrijk", "Aarschot", ["Charleroi", "Luik", "Leuven", "Antwerpen"]) == \
    ['E403 KT', 'E42 TB', 'E42 BC', 'E42 CN', 'E42 NL', 'E40 LLe', 'A3 LB', 'E19 BA', 'N10 AL', 'N10 LAa']
c = time.time()
assert get_quickest_route_via(net, "Oostende", "Aarschot", ["Sydney", "Hemel"]) == \
       ['E40 OB', 'E34 BA', 'YOLOSTRADE', 'YOLOWAY', 'E34 AT', 'StairwayToHeaven', 'BackToReality', 'E34 TA', 'N10 AL',
        'N10 LAa']
d = time.time()
route = get_quickest_route_via(net, "Oostende", "Genk", ["Sydney", "Hel", "Hemel"])
e = time.time()
assert route == ['E40 OB', 'E34 BA', 'YOLOSTRADE', 'YOLOWAY', 'E19 AB', 'E19 BC', 'HighWayToHell', '2deKans', 'E19 CB', 'E19 BA', 'E34 AT', 'StairwayToHeaven', 'BackToReality', 'E34 TA', 'E313 AH', 'N75 HG']


print "tijd: ", b-a
print "tijd2: ", c-b
print "tijd3: ", d-c
print "tijd4: ", e-d
print "OK"
