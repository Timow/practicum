
.PHONY: clean tests
tests:
	python2 oTests.py
	python2 pTests.py
	python2 dTests.py
	python2 lTests.py
	python2 noneTests.py
	python2 Routes_tests.py
	python2 bigTests.py

clean:
	rm -f *.pyc test
