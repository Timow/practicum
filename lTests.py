from Routes import *

roads = (
("AB", "A", "B", 1, 1),
("BD", "B", "D", 1, 1),
("AC", "A", "C", 1, 1),
("CD", "C", "D", 1, 1),
("DE", "D", "E", 4, 1),
("BG", "B", "G", 7, 1),
("EG", "E", "G", 1, 1),
("EF", "E", "F", 1, 1),
("GH", "G", "H", 1, 1),
("FH", "F", "H", 1, 1),
("HF", "H", "F", 1, 1),
("GE", "G", "E", 1, 1),
("ED", "E", "D", 4, 1),
("DB", "D", "B", 1, 1),
("DC", "D", "C", 1, 1)
)
roads2 = (
    ('AB', 'A', 'B', 10, 1),
    ('AC', 'A', 'C', 5, 1),
    ('CB', 'C', 'B', 2, 1),
    ('BD', 'B', 'D', 1, 1),
    ('BF', 'B', 'F', 1, 1),
    ('DF', 'D', 'F', 300, 1),
    ('DC', 'D', 'C', 1, 1)
)
net2 = init_network(roads2)
print "running lTests..."
net = init_network(roads)
assert get_quickest_route_via(net, 'A', 'H', ['G', 'C', 'F']) == ['AB', 'BG', 'GE', 'ED', 'DC', 'CD', 'DE', 'EF', 'FH']
assert get_shortest_route_floyd(net, 'A', 'H')

assert get_quickest_route_via(net2, 'A', 'F', ['B', 'D']) == ['AB', 'BD', 'DC', 'CB', 'BF']
print "OK"
