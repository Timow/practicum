from Routes import *
print "running dTests.."
TEST_ROADS = (
                ("AB", "A", "B", 10.0, 2), 
                ("AB2", "A", "B", 5.0, 1),
                ("BC", "B", "C", 8.0, 3),
                ("CA", "C", "A", 20.0, 4),
            )

TEST_ROUTE = ["AB", "BC", "CA"]

test_net = init_network(TEST_ROADS)

#print get_quickest_route_via(test_net, 'A', 'B', ['C'])
route = get_quickest_route_via(test_net, 'A', 'B', ['C'])
assert route == ['AB', 'BC', 'CA', 'AB2'] or route == ['AB2', 'BC', 'CA', 'AB']
print "OK"
