from Routes import *

print "running pTests..."

test_roads = (
                ("AB", "A", "B", 5.0, 1),
                ("AC", "A", "C", 10.0, 3),
                ("BD", "B", "D", 50.0, 1),
                ("CD", "C", "D", 10.0, 1),
                ("DF", "D", "F", 10.0, 1),
                ("DG", "D", "G", 10.0, 1),
                ("FH", "F", "H", 99.0, 1),
                ("GH", "G", "H", 100.0, 5),
                ("FD", "F", "D", 7.0, 1),
                ("BA", "B", "A", 6.0, 1)
             )
net = init_network(test_roads)


route = get_quickest_route_via(net, 'A', 'H', ['B', 'F'])
assert route == ['AB', 'BA', 'AC', 'CD', 'DF', 'FD', 'DG', 'GH']
assert route_is_contained_in_other_route(['B', 'F'], get_cities_of_route(net, route))

test_2 = (('AC', 'A', 'C', 150.0, 3), ('AB', 'A', 'B', 150.0, 1), ('BC', 'B', 'C', 135.81550808588537, 12), ('BH', 'B', 'H', 159.2294104843483, 12), ('AF', 'A', 'F', 256.42909866929034, 12), ('CG', 'C', 'G', 118.38848248388999, 12), ('BF', 'B', 'F', 148.11923737824756, 12), ('CH', 'C', 'H', 169.15903366451838, 12), ('GA', 'G', 'A', 227.35934168316408, 12), ('FH', 'F', 'H', 250.0, 1), ('HA', 'H', 'A', 283.0898800525936, 12), ('GH', 'G', 'H', 250.0, 5))

net_2 = init_network(test_2)
assert get_quickest_route_via(net_2, 'A', 'G', ['B', 'F']) == ['AB', 'BF', 'FH', 'HA', 'AC', 'CG']
assert get_quickest_route_via(net_2, 'A', 'G', ['F', 'B']) == ['AF', 'FH', 'HA', 'AB', 'BC', 'CG']
print "OK"
