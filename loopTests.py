from Routes import *

roads = (
    ('AB', 'A', 'B', 10, 1),
    ('BD', 'B', 'D', 10, 1),
    ('DB', 'D', 'B', 10, 1),
    ('BC', 'B', 'C', 10, 1),
    ('DE', 'D', 'E', 10, 1),
    ('ED', 'E', 'D', 10, 1)
)

net = init_network(roads)
print "testing loops.."
assert get_quickest_route_via(net, 'A', 'C', []) == ['AB', 'BC']
assert get_quickest_route_via(net, 'A', 'C', ['E']) == ['AB', 'BD', 'DE', 'ED', 'DB', 'BC']
assert get_quickest_route_via(net, 'A', 'C', ['D']) == ['AB', 'BD', 'DB', 'BC']
print "OK"

