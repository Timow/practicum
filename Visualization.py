from Tkinter import *
from math import sqrt
from random import randint
import time

CANVAS_HEIGHT = 600
CANVAS_WIDTH = 800

cities = {}
roads = {}
route = []

START_ROADS = (('AC', 'A', 'C', 150.0, 3), ('AB', 'A', 'B', 150.0, 1), ('BC', 'B', 'C', 135.81550808588537, 12), ('BH', 'B', 'H', 159.2294104843483, 12), ('AF', 'A', 'F', 256.42909866929034, 12), ('CG', 'C', 'G', 118.38848248388999, 12), ('BF', 'B', 'F', 148.11923737824756, 12), ('CH', 'C', 'H', 169.15903366451838, 12), ('GA', 'G', 'A', 227.35934168316408, 12), ('FH', 'F', 'H', 250.0, 1), ('HA', 'H', 'A', 283.0898800525936, 12), ('GH', 'G', 'H', 250.0, 5))

# window objects:
canvas = None
drag_obj = None
label_status = None
text_input = None

import_text = None
root2 = None

def norm(x, y):
    return sqrt(x*x + y*y)

def set_status(t):
    label_status.config(text=t)
def get_text():
    t = text_input.get()
    text_input.delete(0, END)
    return t

def find_city(x, y):
    for city in cities.values():
        if (city[1]+15-x)**2 + (city[2]+15-y)**2 <= 255:
            return city
    return None


# state control:
MAIN_STATE = 0
CREATE_CITY_STATE = 1
NAME_CITY_STATE = 2
SELECT_CITY_STATE = 3
CREATE_ROAD_STATE = 4
SPEED_ROAD_STATE = 5

state = MAIN_STATE
state_obj = None


def change_state(new_state, new_obj=None):
    global state, state_obj
    state = new_state
    state_obj = new_obj
    if new_state == CREATE_CITY_STATE:
        set_status("click on canvas to position city")
    elif new_state == NAME_CITY_STATE:
        text_input.focus_set()
        set_status("name the city")
    elif new_state == MAIN_STATE:
        set_status("click and drag to reposition city")
        draw()
    elif new_state == SELECT_CITY_STATE:
        set_status("selected: " + new_obj[0])
        draw()
    elif new_state == CREATE_ROAD_STATE:
        set_status("select ending city")
    elif new_state == SPEED_ROAD_STATE:
        set_status("enter speed")

def draw():
    canvas.delete(ALL)
    if state == NAME_CITY_STATE:
        draw_oval(state_obj)
    elif state == CREATE_CITY_STATE and drag_obj != None:
        draw_oval(drag_obj, 'red')
    elif state == CREATE_ROAD_STATE and drag_obj != None and len(drag_obj) == 4:
        canvas.create_line(drag_obj[0], drag_obj[1], drag_obj[2], drag_obj[3])
    elif state == SPEED_ROAD_STATE:
        draw_road(("test_obj", state_obj[0], state_obj[1][0], 10, 10))
    for r in roads.values():
        draw_road(r)
    for c in cities.values():
        draw_city(c)


def get_all_roads(city):
    ret = []
    for i in roads.values():
        if city in i[1:3]:
            ret.append(i)
    return ret

def place_roads(new_roads):
    global roads, cities
    all_cities = set()
    roads = {}
    for road in new_roads:
        roads[road[0]] = road
        all_cities.add(road[1])
        all_cities.add(road[2])
    all_cities = list(all_cities)
    print all_cities
    cities = {}
    for city in all_cities:
        cities[city] = (city, randint(0, CANVAS_WIDTH-30), randint(0, CANVAS_HEIGHT-30))

def draw_oval(pos, color="white"):
    return canvas.create_oval((pos[0], pos[1], pos[0]+30, pos[1]+30), fill=color)

def draw_city(city):
    if state == SELECT_CITY_STATE and state_obj[0] == city[0]:
        o = draw_oval(city[1:3], "blue")
    else:
        o = draw_oval(city[1:3])
    t = canvas.create_text((city[1]+15, city[2]+15), text=city[0])

def draw_road(road):
    c1 = cities[road[1]]
    c2 = cities[road[2]]
    length = norm(c1[1]-c2[1], c1[2]-c2[2])
    perc = 1 - (30/length)
    mid_x = int((c1[1]+15)*(1-perc)+(c2[1]+15)*perc)
    mid_y = int((c1[2]+15)*(1-perc)+(c2[2]+15)*perc)
    if road[0] in route:
        canvas.create_line(c1[1]+15, c1[2]+15, mid_x, mid_y, fill="blue")
    else:
        canvas.create_line(c1[1]+15, c1[2]+15, mid_x, mid_y)
    canvas.create_line(mid_x, mid_y, c2[1]+15, c2[2]+15, fill="red", width=2)

def add_city(city):
    cities[city[0]] = city

def add_road(city1, city2, speed):
    i = None
    j = 1
    while i == None or i in roads:
        i = city1[0][0:j] + city2[0][0:j]
        j += 1
    dist_2 = (city1[1]-city2[1])**2 + (city1[2]-city2[2])**2
    dist = sqrt(dist_2)
    roads[i] = (i, city1[0], city2[0], dist, speed)

# callbacks:

def clear_callback():
    global route, roads, cities
    cities = {}
    roads = {}
    route = []
    change_state(MAIN_STATE)

def create_city_callback():
    if state == MAIN_STATE:
        change_state(CREATE_CITY_STATE)
    else:
        set_status("deselect city fist")

def dump_callback():
    if state == MAIN_STATE:
        print tuple(roads.values())
        set_status("see command prompt")
    else:
        set_status("deselect city first")

def create_road_callback():
    if state == SELECT_CITY_STATE and len(cities) > 1:
        change_state(CREATE_ROAD_STATE, state_obj[0])
    else:
        set_status("select starting city first")

def delete_city():
    if state == SELECT_CITY_STATE:
        cities.pop(state_obj[0])
        r = []
        for road in roads.values():
            if state_obj[0] in road[1:3]:
                r.append(road[0])
        for road in r:
            del roads[road]
        change_state(MAIN_STATE)
    else:
        set_status("select city first")

def import_done():
    text = import_text.get('0.0', END)
    new_roads = tuple(eval(text))
    place_roads(new_roads)
    root2.destroy()
    change_state(MAIN_STATE)
    step_callback()

def import_cities():
    global import_text, root2
    if state == MAIN_STATE:
        root2 = Tk()
        import_text = Text(root2)
        import_text.pack(side=LEFT)
        b = Button(root2, text="OK", command=import_done)
        b.pack(side=LEFT)
        root2.mainloop()
    elif state == SELECT_CITY_STATE:
        set_status("deselect city first")
    else:
        set_status("error, finish first")

def ok_callback(e=None):
    if state == NAME_CITY_STATE:
        name = get_text()
        if name in cities:
            set_status("error, name already chosen")
        else:
            add_city((name, state_obj[0], state_obj[1]))
            change_state(MAIN_STATE)
    elif state == SPEED_ROAD_STATE:
        speed = int(get_text())
        city = cities[state_obj[0]]
        add_road(city, state_obj[1], speed)
        print roads
        change_state(MAIN_STATE)
    else:
        change_state(MAIN_STATE)

def click_canvas_callback(event):
    if state == CREATE_CITY_STATE:
        change_state(NAME_CITY_STATE, (event.x, event.y))
    elif state == CREATE_ROAD_STATE:
        c = find_city(event.x, event.y)
        if c == None:
            set_status("select a city!")
        else:
            change_state(SPEED_ROAD_STATE, (state_obj, c))
    elif state == MAIN_STATE or state == SELECT_CITY_STATE:
        c = find_city(event.x, event.y)
        if c != None:
            change_state(SELECT_CITY_STATE, c)
        else:
            change_state(MAIN_STATE)


def double_canvas_callback(event):
    if state == SELECT_CITY_STATE:
        city = state_obj[0]
        cities[state_obj[0]] = (state_obj[0], event.x-15, event.y-15)
        change_state(MAIN_STATE)
        step_callback(city)

def motion_down_canvas_callback(event):
    if state == SELECT_CITY_STATE:
        city = state_obj
        cities[city[0]] = (city[0], event.x-15, event.y-15)
        step_callback(city[0], False)

def motion_canvas_callback(event):
    global drag_obj
    if state == CREATE_CITY_STATE:
        drag_obj = (event.x, event.y)
        draw()
    if state == CREATE_ROAD_STATE:
        city = cities[state_obj]
        drag_obj = (event.x, event.y, city[1]+15, city[2]+15)
        draw()

last_div = 0
last_filter = None
def step_callback(f=None, rep=True):
    global last_div, last_filter
    c = []
    if f != None:
        last_filter = f
    c = [x for x in cities.values() if x[0] != last_filter]
    div = 0
    cities_vec = []
    for city in c:
        div_C = 0
        tot = [0, 0]
        rs = get_all_roads(city[0])
        for road in rs:
            city2 = None
            if city[0] == road[1]:
                city2 = cities[road[2]]
            else:
                city2 = cities[road[1]]
            x = city2[1] - city[1]
            y = city2[2] - city[2]
            n = norm(x, y)
            dl = n - road[3]
            div_C += abs(dl)
            x = x / n
            y = y / n
            tot[0] += x * dl
            tot[1] += y * dl
        cities_vec.append((city[0], tot[0], tot[1]))
        if len(rs) != 0:
            div += div_C / len(rs)
    for vec in cities_vec:
        x = vec[1] / 6
        y = vec[2] / 6
        city = cities[vec[0]]
        cities[vec[0]] = (city[0], city[1]+x, city[2]+y)
    if len(c) != 0:
        div /= len(c)
    draw()
    if abs(last_div-div) > 0.001:
        last_div = div
        if rep:
            canvas.after(33, step_callback)
    else:
        last_filter = None
        if rep:
            canvas.after(200, step_callback)


def create_window():
    """
        creates the basic window
    """
    global canvas, label_status, text_input

    root = Tk()
 
    canvas = Canvas(root, background="#FFF")
    canvas.config(width=CANVAS_WIDTH, height=CANVAS_HEIGHT)
    canvas.grid(row=0, column=0)
    canvas.bind("<Button-1>", click_canvas_callback)
    #canvas.bind("<Double-1>", double_canvas_callback)
    canvas.bind("<Motion>", motion_canvas_callback)
    canvas.bind("<B1-Motion>", motion_down_canvas_callback)

    frame1 = Frame(root)
    
    create_city_button = Button(frame1, text="Create City")
    create_city_button.config(command=create_city_callback)
    create_city_button.pack(side=LEFT)
    
    create_road_button = Button(frame1, text="Create Road")
    create_road_button.config(command=create_road_callback)
    create_road_button.pack(side=LEFT)
 
    delete_button = Button(frame1, text="delete city")
    delete_button.config(command=delete_city)
    delete_button.pack(side=LEFT)   

    dump_button = Button(frame1, text="dump")
    dump_button.config(command=dump_callback)
    dump_button.pack(side=LEFT)

    test_button = Button(frame1, text="import")
    test_button.config(command=import_cities)
    test_button.pack(side=LEFT)

    frame1.grid(row=1, column=0)

    frame2 = Frame(root)

    label_input = Label(frame2, text="input: ")
    label_input.pack(side=LEFT)

    text_input = Entry(frame2, width=20)
    text_input.pack(side=LEFT)
    text_input.bind("<Return>", ok_callback)

    button_ok = Button(frame2, text="OK")
    button_ok.config(command=ok_callback)
    button_ok.pack(side=LEFT)

    button_step = Button(frame2, text="clear!")
    button_step.config(command=clear_callback)
    button_step.pack(side=LEFT)

    frame2.grid(row=2, column=0)

    label_status = Label(root, text="stat")
    label_status.grid(row=3, column=0)
    
    change_state(MAIN_STATE)
    place_roads(START_ROADS)
    step_callback() 
    root.mainloop()
create_window()
