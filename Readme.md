GPS practicum 2015
==================

The solution is in Routes.py.

Every *Tests.py file is a separate test case:
    * Routes_tests.py - the given tests.
    * dTests.py - some simple tests over a small network
    * oTests.py - extensive tests for the helper functions.
    * pTests.py - some tests for get_quickets_route_via
    * lTests.py - some tests for when the total route is faster then the route from start -> via1 -> end...
    * loopTests.py - test for loop optimization of get_quickest_route_via
    * noneTests.py - tests for non existing routes or roads.
    * bigTests.py - test for efficiency of a big network

generate_random.py is used to generate big random networks.

Visualization.py is a small program to create and visualize networks.

To run all tests with make: make tests
To clean all pyc files: make clean