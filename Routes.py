# SOME GUIDELINES:
# 1) Please keep the order of the functions in this skeleton and do not re-arrange them.
# 2) You can add as many (useful) helper functions as you prefer, either at the bottom of the file or in between the
#       required functions
# 3) If required, you may extend the function heading with additional parameters.
#       However, make sure that our tests still work!
#       (only add parameters at the end of the parameter list, and make sure that you provide them with a default value)
# 4) You can write your invariants, documentation strings and comments in either Dutch or English.
#       Both are fine, however, be consistent!


#############################
# INITIALISATION OF NETWORK #
#############################

# EXPLANATION OF NETWORK:
# The network is a tuple of network and network_2,
# network is a dictionary mapping road_ids to roads.
# Roads are represented as dictionaries with the keys (id, start, end, length, speed and time).
# Network_2 is a nested dictionary.
# The first dictionary maps the starting cities to a second dictionary.
# This second dictionary has the key 0 with a list of the ids of all roads starting in that city,
# and a dictionary mapping end cities to the shortest road from start to end.
# this allows to get a road from start to end in constant time which speeds up the init stage of floyd.

# The result of the init_network function is a representation of the network of roads
# and will be used throughout the program as such (as parameter 'network').
# Keep in mind that it should be possible to retrieve the properties of a road (distance, speed, cities)
# in constant time.
# 16+ extension: You should extend the network with a representation that enables retrieval of all roads
# departing from a city in constant time. This extension thus implies that the function returns a tuple
# containing the network representations.
def init_network(roads):
    """
        Returns a network of roads.
        A road is a tuple consisting of a road id, the two cities it connects, the length, and the average speed limit.
        The function translates these tuples to a more useful representation that will be used throughout the program.
    """
    network = {}
    all_cities = set()
    network_2 = {}

    # INVARIANT:
    #   Iterate over all roads and build the network by creating a dictionary and setting the id as key in network.
    #   Add the roads start and end to all_cities.
    #   Build the second network by adding the road id to all roads starting from the start city
    #   and adding it to network_2[start][end] if it is faster then the current road between start and end.
    for road in roads:
        # build the road dictionary:
        network[road[0]] = {
            "id": road[0],
            "start": road[1],
            "end": road[2],
            "length": road[3],
            "speed": road[4],
            "time": road[3]/road[4]
        }

        # build all cities:
        all_cities.add(road[1])
        all_cities.add(road[2])

        # build network_2:
        if road[1] in network_2:
            # add to all cities starting from road[1]:
            network_2[road[1]][0].append(road[0])

            # add to the end dictionary:
            if road[2] in network_2[road[1]]:
                # make sure the road is the shortest one:
                if road[3] < network[network_2[road[1]][road[2]]]['length']:
                    network_2[road[1]][road[2]] = road[0]
            else:
                # add new road to start dictionary:
                network_2[road[1]][road[2]] = road[0]
        else:
            # new start city, create dict and list:
            network_2[road[1]] = {}
            network_2[road[1]][road[2]] = road[0]  # for start to end
            network_2[road[1]][0] = [road[0]]  # for all cities.

    # add empty connections between cities:
    # INVARIANT:
    #   iterate over all_cities to add None to the roads that don't exist.
    #   and create an empty list if the start cities isn't already in the network.
    for start in all_cities:
        if start not in network_2:
            network_2[start] = {0: []}  # empty start city
        # INVARIANT:
        #   iterate over all_cities to add all nonexistent connections between start and end.
        for stop in all_cities:
            if stop not in network_2[start]:
                network_2[start][stop] = None  # empty connection

    return network, network_2


#####################################
# ALGORITHM 1: FLOYD SHORTEST ROUTE #
#####################################


# the algorithm assumes no loops (i.e. no road should be used more than once).
# multiple roads between two cities are however possible.
# The lookup function (part 3 of the algorithm) should be implemented as a recursive helper function

def init_distances(network, all_cities):
    """
        creates the distance network, it contains the distances between all cities
        in an double dictionary [start][end] -> (length, (next_city, road_to_next_city).
        All the routes are currently direct (after executing this function).
    """
    route_network = {}
    # INVARIANT:
    #   iterate over all possible starting cities to create the distance network.
    #   Create a new empty dictionary for network[start]
    for start in all_cities:
        route_network[start] = {}
        # INVARIANT:
        #   Iterate over all ending cities, setting the network[start][end]
        #   to a tuple containing the length, next road and the end of the next road
        #   or if the road doesn't exist None or (0, ()) if start and end are the same.
        for end in all_cities:
            road_id = get_road_to(network, start, end)  # get the shortest road
            if start == end:
                route_network[start][end] = (0, ())  # same city -> no distance
            elif road_id is None:
                route_network[start][end] = (None, ())  # no road exists -> None length
            else:
                road = network[0][road_id]  # get the road from the id
                route_network[start][end] = (road['length'], (end, road['id']))
    return route_network


def get_route(route_network, start, end):
    """
       recursively get the next route to build a path from start to end.
    """
    # Are we done?
    if start == end:
        return []  # this just appends an empty list to the building list.
    # nope, so get the next route:
    next_route = route_network[start][end][1]
    if next_route == ():  # no route exists -> return None
        return None
    # recursively get the path from the next route to the end
    rest = get_route(route_network, next_route[0], end)
    # return the path from start to next city + from next route to the end
    return [next_route[1]] + rest


def get_shortest_route_floyd(network, start, destination, excludings=[]):
    """ 
        Returns the shortest route between the given start city and given destination city,
        which avoids all cities in excludings.
        The function calculates the shortest routes between all cities based on the roads in the provided network. 
        Once all shortest routes are computed,
        the function returns the shortest route (as a list of road IDs).
        Cities to be avoided (excludings) are dealt with by the algorithm.
    """

    all_cities = []

    # no list comprehensions so:
    # build all cities excluding the excluded cities:
    # INVARIANT:
    #   iterate over all keys of network[1], append it to the list of all possible cities,
    #   if the city is not in excludings.
    for i in network[1]:
        if i not in excludings:  # remove all cities in excludings
            all_cities.append(i)

    if start not in all_cities or destination not in all_cities:
        return None

    # build the distance network:
    route_network = init_distances(network, all_cities)

    # INVARIANT:
    #   check for each possible via if the road from each possible start to each possible end isn't shorter
    #   if we go pass the via, if so we save this route as the new route from start to end,
    #   and set the next road to visit to the via.
    #   after these loops we have a complete distance matrix from each city to each city.
    for via_city in all_cities:
        for start_city in all_cities:
            for end_city in all_cities:

                cur_dist = route_network[start_city][end_city]  # get current distance
                dist_1 = route_network[start_city][via_city]  # get distance from start to via
                dist_2 = route_network[via_city][end_city]  # get distance from via to start

                if dist_1[0] is not None and dist_2[0] is not None:  # distances exist
                    if cur_dist[0] is None or cur_dist[0] > dist_1[0] + dist_2[0]:  # and are shorter
                        # add distances
                        # next = next of dist_1 -> we go via k
                        route_network[start_city][end_city] = (dist_1[0] + dist_2[0], dist_1[1])

    # lookup route from start to end in the finished distance network:
    return get_route(route_network, start, destination)


##############################################
# ALGORITHM 2: SHORTEST ROUTE INCLUDING VIAS #
##############################################

def test_quickest(network, start, ends, total_time=0, current_route=[], best_time=None, visited_cities=set()):
    """
        gets the quickest route from start via ends to the last end, returns a tuple of (time, route)
    """
    # check if we are longer than the best route:
    if best_time is not None and total_time >= best_time:
        return None  # we already have a better route

    # we are done, get to the next via:
    if start == ends[0]:
        # there are no more vias return the time we needed to complete:
        if len(ends) == 1:
            return total_time, current_route
        # otherwise get the quickest route to the next via:
        return test_quickest(network, start, ends[1:], total_time, current_route, best_time)

    # get all the possible roads:
    all_possible_roads = get_all_roads_starting_from(network, start)
    best_route = []
    # INVARIANT:
    #   The for loop walks over every possible road starting from the current starting city.
    #   It then checks if the road is viable by checking if we have already taken the road or
    #   if we have an unnecessary loop.
    #
    #   The current route is append the road we are trying and the of this road is added to the current time,
    #   the current city (start) is added to the visited cities.
    #   If it finds a way to the next stop and thus to the end it takes the time and compares it to
    #   the current best time. if it is better it uses this route as the current best route.
    #   The loop will end when all possible routes have been tried.
    for i in all_possible_roads:
        # find a viable road, and avoid loops:
        if i not in current_route:
            road = network[0][i]
            if road['end'] not in visited_cities:
                time = road['time']
                # check the time needed to get to the end from this road:
                test = test_quickest(network, road['end'],
                                     ends, total_time + time,
                                     current_route + [i],
                                     best_time, visited_cities | {start})

                # best time ?:
                if test is not None and (best_time is None or test[0] < best_time):
                    best_time = test[0]
                    best_route = test[1]
    # no best time found this is a dead end:
    if best_time is None:
        return None
    # return the best road to the end:
    return best_time, best_route


# The resulting route should not contain loops (each road can only be used once).
# Note that cities might be connected by multiple roads.
def get_quickest_route_via(network, start, destination, vias):
    """ Returns the quickest route from the given start city to the given destination city
        while visiting the intermediate cities (vias) in order. """

    # check for non existing start, destination or vias:
    if start not in network[1] or destination not in network[1]:
        return None

    for via in vias:
        if via not in network[1]:
            return None

    # get the quickest route:
    test = test_quickest(network, start, vias + [destination])
    if test is None:
        return None
    return test[1]


##########################
# ROUTE HELPER FUNCTIONS #
##########################

# Reminder: a route is a list consisting of road IDs

def get_length_of_route(network, route):
    """ Returns the total distance of the given route. """
    tot = 0
    # INVARIANT:
    #   tot is the sum of lengths of all roads in the route.
    #   The loop gets the time of each road adding it to tot.
    #   If get_length returns none the road doesn't exists so we return None.
    #   The for loop ends when all roads of the route have been added.
    for road in route:
        current = get_length(network, road)
        if current is None:
            return None
        tot += current
    return tot


def get_time_of_route(network, route):
    """ 
        Returns the estimated time of the route.
        In hours.
    """
    tot = 0
    # INVARIANT:
    #   the for loop walks over all roads and adds the time to te total time.
    #   if the time is None, the road doesn't exist.
    #   tot is the sum of all the times it takes to take a road in the route.
    for road in route:
        current = get_time(network, road)
        if current is None:
            return None
        tot += current
    return tot


def get_cities_of_route(network, route):
    """ Returns a list that contains all visited cities (strings) in order of the given route """
    cities = []
    # INVARIANT:
    #   Iterate over all roads in the route adding the start to visited cities.
    #   If the road doesn't exist return None.
    #   Also checks if we are the last road if so we add the end to the list.
    for road in route:
        if road not in network[0]:
            return None
        r = network[0][road]  # get the road info
        cities.append(r['start'])

        if road == route[-1]:  # are we the last road -> add the destination
            cities.append(r['end'])

    return cities


def cities_occur_in_route(network, cities, route):
    """ Returns whether the entire collection of cities is visited during the route.
        Note that the cities do not necessarily have to be visited in order."""
    route_cities = get_cities_of_route(network, route)
    if route_cities is None:
        return None
    # INVARIANT:
    #   the loop returns false if any city of the collection is not in the route.
    #   if the loop ends without returning, all the cities must have been in the route.
    for city in cities:
        if city not in route_cities:
            return False
    return True


def route_contains_loop(route):
    """ Return whether a route contains a loop. A loop is defined as a road that occurs more than once. """
    # INVARIANT:
    #   The loop returns True if any of the roads in the route
    #   appears more than one time.
    #   If the loop ends then all roads must only be present once so we return False.
    for road in route:
        if route.count(road) > 1:
            return True
    return False


def route_is_contained_in_other_route(route, target):
    """
        Returns whether each road of the first route is contained in the target route.
        The order of the route should be respected, but the roads should not necessarily be used consecutively.
        (for example: ["AB", "BC", "CD"] is contained in ["ZA", "AX", "XA", "AB", "BY", "YW", "WY", "YB", "BC", "CD"])
    """

    target_sub = target
    # INVARIANT:
    #   Checks whether each element of the route appears in the target in the correct order.
    #   It checks if the current road is in the sub list of target.
    #   If not the function returns false.
    #   Otherwise we set the target to the sub list starting from the found road.
    #   This ensures that all roads appear in target in the correct order.
    for road in route:
        if road not in target_sub:
            return False
        index = target_sub.index(road)
        target_sub = target_sub[index:]
    return True


def is_correct_route(network, route):
    """ Determines whether a given route is correct by examining
        whether the destination city of each road equals the origin city of the next road """
    last_destination = get_end(network, route[0])
    if last_destination is None:
        return None
    i = 1
    # INVARIANT:
    #   Checks if the start of the next road is equal to the end of the current road.
    #   If not the function returns false.
    #   Also check if each road exists by checking if get_start or get_end returns None.
    # VARIANT: len(route) - i
    while i < len(route):
        departure = get_start(network, route[i])
        if departure is None:
            return None
        if last_destination != departure:
            return False
        last_destination = get_end(network, route[i])
        if last_destination is None:
            return None
        i += 1
    return True


#########################
# ROAD HELPER FUNCTIONS #
#########################

# ALL FUNCTIONS NEED TO BE EXECUTED IN CONSTANT TIME

def get_start(network, road_id):
    """ Returns the original city of the road identified by road_id """
    if road_id not in network[0]:
        return None
    return network[0][road_id]['start']


def get_end(network, road_id):
    """ Returns the destination city of the road identified by road_id """
    if road_id not in network[0]:
        return None
    return network[0][road_id]['end']


def get_length(network, road_id):
    """ Returns the distance of the road identified by road_id """
    if road_id not in network[0]:
        return None
    return network[0][road_id]['length']


def get_speed(network, road_id):
    """ Returns the allowed speed of the road identified by road_id """
    if road_id not in network[0]:
        return None
    return network[0][road_id]['speed']


def get_time(network, road_id):
    """ Returns the estimated time of the road identified by road_id """
    if road_id not in network[0]:
        return None
    return network[0][road_id]['time']


#########################
# CITY HELPER FUNCTIONS #
#########################

# the second network stores a dict in the form [start][end] -> list of roads,
# there is an extra key in the end dictionary (0) which contains all road starting in the start city.
# this allows to get a road from start to end in constant time.
# The network is also build so the shortest road will be the first element of the list.
# It is not sorted, the first element is just the shortest.

# 16+ requirement: this function should execute in constant time
def get_all_roads_starting_from(network, city):
    """ Returns a list containing all the roads that depart in the given city """
    if city not in network[1]:
        return None
    return network[1][city][0]


# BONUS: also returns in constant time:
def get_road_to(network, start, end):
    """ Returns the shortest (direct) road from the given start city to the given end city.
        When no such road exists, return None. """
    if start not in network[1] or end not in network[1]:
        return None
    return network[1][start][end]
