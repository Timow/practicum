from Routes import *
print "running oTests..."

TEST_ROADS = (
                ("AB", "A", "B", 10.0, 2), 
                ("AB2", "A", "B", 5.0, 1),
                ("BC", "B", "C", 8.0, 3),
                ("CA", "C", "A", 20.0, 4),
                ("CD", "C", "D", 5.0, 5),
                ("DA", "D", "A", 5.0, 5),
                ("AC", "A", "C", 10.0, 3)
            )

TEST_ROUTE = ["AB", "BC", "CA"]

test_net = init_network(TEST_ROADS)

assert get_length_of_route(test_net, TEST_ROUTE) == 10.0 + 8.0 + 20.0

assert get_time_of_route(test_net, TEST_ROUTE) == 10.0/2.0 + 8.0/3.0 + 20.0/4.0

assert get_cities_of_route(test_net, TEST_ROUTE) == ['A', 'B', 'C', 'A']

assert cities_occur_in_route(test_net, ["A", "B"], TEST_ROUTE)
assert not cities_occur_in_route(test_net, ["D"], ["AB", "BC"])

assert not route_contains_loop(TEST_ROUTE)
assert route_contains_loop(["AB", "BC", "CA", "AB"])

assert route_is_contained_in_other_route(["AB", "CA"], TEST_ROUTE)
assert not route_is_contained_in_other_route(["BC", "CD"], TEST_ROUTE)

assert is_correct_route(test_net, TEST_ROUTE)
assert is_correct_route(test_net, ["BC", "CD"])

assert not is_correct_route(test_net, ["AB", "BC", "AB"])

print "OK" 
